require('babel-register')
require("babel-polyfill");
var path = require("path");
var express = require("express");
var jwt = require('express-jwt');
var bodyParser = require('body-parser');
var tokenManager = require('./config/token_manager');
var secret = require('./config/secret');
//Routes
var routes = {};
routes.users = require('./route/user.js');


var app = express();
var serverPort = process.env.PORT || 3000;
app.listen(serverPort, "localhost", function (err) { // 192.168.7.148
  if (err) {
    console.log(err);
    return;
  }
  console.log("Listening at http://localhost:" + serverPort);
});

app.use(function(req, res, next) {
  // res.header("Access-Control-Allow-Origin", "*"); 
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  // res.header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE');
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'btb')))

// index.html//默认跳转主页
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});


app.get('/node/user/info',  tokenManager.verifyToken, routes.users.userInfo);


// app.post('/superaplipay/pay', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.superAplipay.pay);

process.on('uncaughtException', function(err){
  console.log(err);
})

