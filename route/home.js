var redisClient = require('../config/redis_database').redisClient;
var moment      = require('moment');


// 获取访问量
exports.getVist = function(req, res) {
	var params = req.query;
	var today = moment(new Date()).format("YYYYMMDD");
	if(params.source==0){
		redisClient.get(today, function (err, rank) {
			res.json(rank);
		})
	}else{
		redisClient.get(today+params.source, function (err, rank) {
			res.json(rank);
		})
	}
}


// 统计访问量
exports.vist = function(req, res) {
	var params = req.query;
	var today = moment(new Date()).format("YYYYMMDD");
	add(today);

	if(params.source!=0){
		add(today+params.source);
	}
	return res.json({status:200, msg:'ok'});
	
}

function add(today){
	// 统计总额
	redisClient.get(today, function (err, rank) {
		if(rank==null){
			redisClient.set(today,0);
		}
		redisClient.incrby(today, 1);
	})
}