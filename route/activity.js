var request = require('request');
var md5 = require('md5');
var db = require('../config/mongo_database');
var jwt = require('jsonwebtoken');
var secret = require('../config/secret');
var redisClient = require('../config/redis_database').redisClient;
var tokenManager = require('../config/token_manager');
var logger = require('../config/log4js').logger('access');


// app: 首页活动列表
exports.appActlist = function(req, res){
    db.activityModel.find({status:'1', hot:'1', isend:'1'}, 
      {name:1, price:1, btype:1, img:1, member:1,start:1}, function(err, list){
        res.json({status:200, list:list, msg:'查询成功'}); 
    })
}



//新增活动
exports.addNewActivity = function(req,res){ 
    
    var  member   = req.body.member    || ''; //人数
    var  name     = req.body.name      || ''; //活动名字
    var  price    = req.body.price     || ''; //活动价格
    var  resultlen= req.body.resultlen || ''; //开奖号码长度
    var  img      = req.body.img       || ''; //活动图
    var  hot      = req.body.hot       || '';//热度
    var  type     = req.body.type      || ''; //活动类型 0， 1 ，2 。。。
    var  btype    = req.body.btype     || ''; //1BTC 2ETH 3ETC 4LTC 5DOGE 6YBC
    var  content  = req.body.content   || '';//活动内容
    var  ok       = req.body.ok        || '';//开奖条件
    var  condition= req.body.condition || '';//活动条件
    var  actstatus= req.body.actstatus || '';//活动状态1未开奖，2已开奖
    var  start    = req.body.start     || '';//开始时间
    var  end      = req.body.end       || '';//结束时间
    var  isend    = req.body.isend     || '';//是否结束 1未结束 0已结束
    var  status   = req.body.status    || '';//活动状态// 状态1启用，0禁用
    var  postlog  = req.body.postlog   || '';//活动日志
    var  remark   = req.body.remark    || '';//开奖说明
    var  fee      = req.body.fee       || '';// 手续费
    


    if (member == '' 
        || name == '' 
        || price == ''
        || type == '' 
        || content == '' 
        || ok == '' 
        || condition == '' 
        || start == '' 
        || end == '') {
		return res.json({status:400, msg:'参数为空'});
	}
    var activity = new db.activityModel();
    		activity.member    = member;
    		activity.name      = name;
        activity.price     = price;
        activity.resultlen = resultlen;
        activity.img       = img;
    		activity.hot       = hot;
    		activity.type      = type;
    		activity.btype     = btype;
    		activity.content   = content;
    		activity.ok        = ok;
    		activity.condition = condition;
    		activity.actstatus = actstatus;
    		activity.start     = start;
    		activity.end       = end;
        activity.isend     = isend;
    		activity.status    = status;
        activity.postlog   = postlog;
        activity.remark    = remark;
        activity.fee       = fee;

    		activity.save(function(err) {
          if(err)
            return res.json({status:500, msg:'保存失败'});
				  return res.json({status:200, msg:'保存成功'});
			});
};

//删除活动
exports.removeActivity = function (req,res){
	var  status = '1';
	var activityId = req.body._id;
	db.activityModel.findOne({_id:activityId}, function (err, act) {
		if(err) res.sendStatus(500); 
		if(null!=act){
			db.activityModel.update({_id:activityId}, {$set:status}, function(err, activity){ // 执行变更
			res.sendStatus(200); 
			})
		}
	});

};

//修改活动
exports.updateActivity = function (req,res){
	  var _id      = req.body._id;
	  var  member    = req.body.member   || ''; //人数
    var  name     = req.body.name      || ''; //活动名字
    var  price    = req.body.price     || ''; //活动价格
    var  resultlen= req.body.resultlen || ''; //开奖号码长度
    var  img      = req.body.img       || ''; //活动图
    var  hot      = req.body.hot       || '';//热度
    var  type     = req.body.type      || ''; //活动类型 0， 1 ，2 。。。
    var  btype    = req.body.btype     || ''; //1BTC 2ETH 3ETC 4LTC 5DOGE 6YBC
    var  content  = req.body.content   || '';//活动内容
    var  ok       = req.body.ok        || '';//开奖条件
    var  condition= req.body.condition || '';//活动条件
    var  actstatus= req.body.actstatus || '';//活动状态1未开奖，2已开奖
    var  start    = req.body.start     || '';//开始时间
    var  end      = req.body.end       || '';//结束时间
    var  isend    = req.body.isend     || '';
    var  status   = req.body.status    || '';//活动状态// 状态1启用，0禁用
    var  postlog  = req.body.postlog   || '';//活动日志
    var  remark   = req.body.remark    || '';//开奖说明
    var  result   = req.body.result    || null;//开奖结果
    var  fee      = req.body.fee       || '';// 手续费

    db.activityModel.findOne({_id:_id}, function (err, act) {
		if(err || null==act) return res.json({status:500, msg:'未查找到数据'});
		if(null!=act){
			db.activityModel.update({_id:_id},
               {$set:{member:member,
               name:name,
               price:price,
               resultlen:resultlen,
               img:img,
               hot:hot,
               type:type,
               btype:btype,
               content:content,
               ok:ok,
               condition:condition,
               actstatus:actstatus,
               start:start,
               end:end,
               isend:isend,
               status:status,
               postlog:postlog,
               remark:remark,
               result:JSON.parse(result),
               fee:fee}}, function(err){ // 执行变更
        if(err)
          return res.json({status:500, msg:'更新失败'});

				return res.json({status:200, msg:'更新成功'});
			})
		}
	});
};


//根据热门度查询活动活动
exports.searchHotActive = function(req,res){
	var status = req.body.status;
	var  hot = req.body.hot;
	db.activityModel.find({status:status,hot:hot}, function(err, list){
		res.json(list); 
	});
};

//后台活动管理
exports.backActlist = function(req,res){
    var actstatus  = req.body.actstatus || '';
    var btype      = req.body.btype     || '';
    var type       = req.body.type      || '';
    var hot        = req.body.hot       || '';
    var isend      = req.body.isend     || '';
    var pages      = req.body.pages     || 0;
    
    var where = {};
    if(actstatus!='') where.actstatus = actstatus;
    if(type!='')      where.type      = type;
    if(btype!='')     where.btype     = btype;
    if(hot!='')       where.hot       = hot;
    if(isend!='')     where.isend     = isend;

    db.activityModel.count(where, function(err, count){
      db.activityModel
      .where(where)
      .skip(pages*12)
      .limit(12)
      .sort({ updated : -1 })
      .exec(function(err, list){
        res.json({status:200, count:count, list:list});
      })
    }).sort({'updated':-1});
}