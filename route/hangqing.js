var moment       = require('moment');
var request      = require('request');
var db           = require('../config/mongo_database');

//
exports.contact = function(req, res){
	var name    = req.body.name || '';
	var email   = req.body.email || '';
	var content = req.body.content || '';

	var news = new db.newsModel();
	news.name    = name
	news.email   = email
	news.content = content

	news.save(function(err){
		res.json({status:200, msg:'提交成功'})
	})
}

// 首页热度币查询
exports.hotlist = function(req, res){
	db.blistModel
		.where({status:'0'})
		.limit(5)
		.sort({rank:1})
		.exec(function(err, list){
			return res.json({status:200, list:list});
		})
}

// 玩客币行情展示
exports.wkblist = function(req, res){
	db.bcoinModel.findOne({btype:'WKB'}, function(err, data){
		return res.json({status:200, data:data.html})
	})
}


// 列表页面
exports.blist = function(req, res){
	var pages  = req.body.pages || 0;
	var symbol = req.body.symbol|| '';
	var price_cny = req.body.price_cny|| '';
	var market_cap_cny = req.body.market_cap_cny|| '';
	var available_supply = req.body.available_supply|| '';
	var available_supply_d = req.body.available_supply_d|| '';
	var sorName = req.body.sorName || '';
	var sorVal  = req.body.sorVal  || '';

	var where = {status:'0'};
	if(symbol!=null && symbol!='')
		where.symbol = symbol

	var sort = {rank:1};
	if(sorName!=null && sorVal!=''){
		sort[sorName] = sorVal
		delete sort.rank
	}

	console.log(sort)
	
	db.blistModel.count(where, function(err, count){
		db.blistModel
		.where(where)
		.skip(pages*100)
		.limit(100)
		.sort(sort)
		.exec(function(err, list){
			return res.json({status:200, count:count, list:list});
		})
	})
}