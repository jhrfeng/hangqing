var log4js = require('log4js');
log4js.configure({
	appenders: [
		{
			"type": "console"
		},
	    {
		    type: 'dateFile',
		    filename: 'logs/date.log',
		    pattern: '-yyyy-MM-dd.log',
		    alwaysIncludePattern: true,
		    category: 'access'
		}
	],
	replaceConsole: true
});

exports.logger=function(name){
    var logger = log4js.getLogger(name);
    logger.setLevel('INFO');
    return logger;
}