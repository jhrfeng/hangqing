var mongoose = require('mongoose');
var md5 = require('md5');
var mongodbURL = 'mongodb://127.0.0.1:27017/hangqing';
var mongodbOptions = { };

mongoose.connect(mongodbURL, mongodbOptions, function (err, db) {
    if (err) { 
        console.log('Connection refused to ' + mongodbURL);
        console.log(err);
    } else {
        console.log('Connection successful to: ' + mongodbURL);
    }
});

var Schema = mongoose.Schema;

// User schema
var User = new Schema({
    username: { type: String, required: true, unique: true },    // 手机号
    password: { type: String, required: true },                  // 密码 md5
    header:   { type: String, default: ""    },                  // 头像
    type:     { type: String, default: "0"   },                  // 类型
    property: { type: Array                  },                  // 钱包资产
    asset:    { type: Number, default: 0     },                  // APP资产
    realinfo: { type: Object                 },                  // 实名信息
    otherinfo:{ type: Object                 },                  // 其他信息
    tradepwd: { type: String, default: ""    },                  // 交易密码
    nick:     { type: String, default: ""    },                  // 昵称
    status:   { type: String, default: "1"   },
    updated:  { type: Date, default: Date.now},
    created:  { type: Date, default: Date.now}
});


// 币种种类
var Blist = new Schema({
    id:                 { type: String,  required: true, unique: true}, 
    name:               { type: String,  default: "" },            
    symbol:             { type: String,  default: "" },
    rank:               { type: Number,  default: 0 },
    price_usd:          { type: Number,  default: 0 },
    price_btc:          { type: Number,  default: 0 },
    esh_volume_usd:     { type: Number,  default: 0 },
    market_cap_usd:     { type: Number,  default: 0 },
    available_supply:   { type: Number,  default: 0 },
    total_supply:       { type: Number,  default: 0 },
    max_supply:         { type: Number,  default: 0 },
    percent_change_1h:  { type: Number,  default: 0 },
    percent_change_24h: { type: Number,  default: 0 },
    percent_change_7d:  { type: Number,  default: 0 },
    last_updated:       { type: String,  default: "" },
    price_cny:          { type: Number,  default: 0 },
    esh_volume_cny:     { type: Number,  default: 0 },
    market_cap_cny:     { type: Number,  default: 0 },
    created:            { type: Date,    default: Date.now},
    status:             { type: String,  default: "0"}                 // 状态1启用，0禁用
})

// 币种种类
var Bstore = new Schema({
    key:                { type: String, required: true, unique: true }, 
    id:                 { type: String,  default: "" }, 
    name:               { type: String,  default: "" },            
    symbol:             { type: String,  default: "" },
    rank:               { type: Number,  default: 0 },
    price_usd:          { type: Number,  default: 0 },
    price_btc:          { type: Number,  default: 0 },
    esh_volume_usd:     { type: Number,  default: 0 },
    market_cap_usd:     { type: Number,  default: 0 },
    available_supply:   { type: Number,  default: 0 },
    total_supply:       { type: Number,  default: 0 },
    max_supply:         { type: Number,  default: 0 },
    percent_change_1h:  { type: Number,  default: 0 },
    percent_change_24h: { type: Number,  default: 0 },
    percent_change_7d:  { type: Number,  default: 0 },
    last_updated:       { type: String,  default: "" },
    price_cny:          { type: Number,  default: 0 },
    esh_volume_cny:     { type: Number,  default: 0 },
    market_cap_cny:     { type: Number,  default: 0 },
    created:            { type: Date,    default: Date.now},
    status:             { type: String,  default: "0"}                 // 状态1启用，0禁用
})


// 币种种类
var Bcoin = new Schema({
    btype:    { type: String, required: true, unique: true}, // 币种类型
    name:     { type: String, default: 0     }, 
    html:     { type: String, default: ""   },        // 币种名称
    status:   { type: String, default: "1"   },
    created:  { type: Date, default: Date.now}
})

// 币种种类
var News = new Schema({
    name:      { type: String, required: true },          
    email:     { type: String, default: 0     },         
    content:   { type: String, default: "1"   },
    created:   { type: Date, default: Date.now}
})



//  对账单
var Balance = new Schema({
    orderid:  { type: String, required: true, unique: true},     // 订单id
    userid:   { type: String, required: true },                  // 用户ID
    username: { type: String, required: true },                  // 用户账号
    nick:     { type: String, default: ""    },                  // 昵称
    type:     { type: String                 },                  // 0 购买 1 充值 2提现 3赎回到账
    btype:    { type: String, default: "-1"  },                  // 币种类型
    content:  { type: String, default: ""    },                  // 账单内容
    countn:   { type: Number                 },                  // 金额正负，用于累计相加计算
    pay:      { type: Number                 },                  // 金额
    tradeno:  { type: String, default: ""    },                  // 交易流水号凭证
    saddr:    { type: String, default: ""    },                  // 发送地址
    raddr:    { type: String, default: ""    },                  // 接受地址
    flag:     { type: String, default: ""    },                  // 标识
    ps:       { type: String, default: "0"   },                  // 0转入，1转出
    status:   { type: String, default: "0"   },                  // 状态1启用，0禁用
    updated:  { type: Date, default: Date.now},
    created:  { type: Date, default: Date.now}
});


//  log
var Log = new Schema({
    userid:   { type: String, required: true   },                  // 用户ID
    content:  { type: String, default: ""      },    
    detail:   { type: Object                   },   
    updated:  { type: Date,   default: Date.now},
    created:  { type: Date,   default: Date.now}
});





// Bcrypt middleware on UserSchema
User.pre('save', function(next) {
    var user = this;
    if (!user.isModified('password')) return next();
    user.password = md5(user.password);
    next();
});

//Password verification
User.methods.comparePassword = function(password, cb) {
    if(md5(password)==this.password){
        cb(true);
    }else{
        return cb(false);
    }
};


//Define Models
var userModel      = mongoose.model('User', User);
var balanceModel   = mongoose.model('Balance', Balance);
var logModel       = mongoose.model('Log', Log);
var bcoinModel     = mongoose.model('Bcoin', Bcoin);
var blistModel     = mongoose.model('Blist', Blist);
var bstoreModel    = mongoose.model('Bstore',Bstore);
var newsModel      = mongoose.model('News',News);


// Export Models
exports.userModel      = userModel;
exports.balanceModel   = balanceModel;
exports.logModel       = logModel;
exports.bcoinModel     = bcoinModel;
exports.blistModel     = blistModel;
exports.bstoreModel    = bstoreModel;
exports.newsModel      = newsModel;



