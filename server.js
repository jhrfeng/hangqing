var path           = require("path");
var express        = require("express");
var jwt            = require('express-jwt');
var bodyParser     = require('body-parser');
var log4js         = require('log4js');
var tokenManager   = require('./config/token_manager');
var secret         = require('./config/secret');
var logjs          = require('./config/log4js');

//Routes
var routes = {};
routes.schedule    = require('./route/schedule.js');
routes.hangqing    = require('./route/hangqing.js');
routes.home        = require('./route/home.js');



var app = express();
var serverPort = process.env.PORT || 3000;
app.listen(serverPort, "0.0.0.0", function (err) { // 192.168.7.148
  if (err) {
    console.log(err);
    return;
  }
  console.log("Listening at http://localhost:" + serverPort);
});

log4js.connectLogger(logjs.logger('access'), {level:'info', format:':method :url'})


app.use(function(req, res, next) {
  // res.header("Access-Control-Allow-Origin", "*"); 
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  // res.header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE');
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'html')))

// index.html//默认跳转主页
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});


// 热度
app.get('/hangqing/hotlist', routes.hangqing.hotlist); 

// 所有列表
app.post('/hangqing/blist',   routes.hangqing.blist); 

// 所有列表
app.post('/hangqing/contact',   routes.hangqing.contact); 

// 玩客币统计
app.get('/hangqing/wkb', routes.hangqing.wkblist);

// 统计访问量
app.get('/home/vist', routes.home.vist); 

// 查看访问量
app.get('/home/getVist', routes.home.getVist); 







process.on('uncaughtException', function(err){
  console.log(err);
})

