function totalPage(pages){
	return Math.ceil(pages/12)
}

function switchPage(pages){
	var newNum = (pages+1)/5  // 每页10条一共5列
	if(String(newNum).indexOf(".")>-1){
		newNum = Number(newNum.toFixed(0))
	}
	if(newNum > 0){
		return newNum*4
	}
	return newNum
}