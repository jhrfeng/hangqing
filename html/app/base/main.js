globalConfig = {
	web:'https://www.btb000.com',
	// web: 'http://127.0.0.1:3200'
};


(function(document) {

	globalConfig.app = angular.module('app', ['ui.router','oc.lazyLoad']);

	var routerList = [
		'app/home/home.js',
	];
	
	var str = '';
	routerList.forEach(function(router) {
		str += '<script src="' + router + '"></script>';
	});
	document.write(str);

}(document));


angular.module('app').config(['$urlRouterProvider', function($urlRouterProvider) {
	$urlRouterProvider.when('', '/home');
}]);

angular.module('app').controller('rootCtrl',['$rootScope',function($rootScope){
	
   
}]);

(function(document) {
	var libList = [
		'app/base/other/filter.js',
		'app/base/other/httpUtil.js',
	];
	var str = '';
	libList.forEach(function(lib) {
		str += '<script src="' + lib + '"></script>';
	});
	document.write(str);
}(document));


//延迟加载
var lazyloadlib = [
	'app/base/directive/datepicker.js',
];