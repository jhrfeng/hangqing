angular.module('app').filter('trustHtml', function ($sce) {
    return function (input) {
        return $sce.trustAsHtml(input);
    }
}).filter('fixed', function () {
    return function (value) {
        if(Number(value)>100)
          return Number(value).toFixed(0)
        else
          return Number(value).toFixed(4)
    }
}).filter('btype', function () {
    return function (type) {
       if(type == 0)
       		return 'BTC'
       	else if(type == 1)
       		return 'ETH'
       	else if(type == 2)
       		return 'BTG'
        else if(type == 3)
          return 'LTC'
        else if(type == 4)
          return 'CNY'
    }
}).filter('price', function () {
    return function (num) {
        var moneyUnits = ["元", "万元", "亿元", "万亿"]
        var dividend = 10000
        var curentNum = Number(num) //转换数字 
        var curentUnit = moneyUnits[0] //转换单位 
        for (var i = 0; i <4; i++) { 
          curentUnit = moneyUnits[i] 
          if(strNumSize(curentNum)<5){
             break; 
          } 
          curentNum = curentNum / dividend
         } 
         var m = {num: 0, unit: ""} 
         m.num = curentNum.toFixed(2) 
         m.unit = curentUnit 
         return m.num+m.unit
    }
    function strNumSize(tempNum){ 
      // console.log(empNum.toString())
        var stringNum = tempNum.toString()
        var index = stringNum.indexOf(".") 
        var newNum = stringNum 
        if(index!=-1){ 
          newNum = stringNum.substring(0,index) 
        } 
        return newNum.length
    }

});

