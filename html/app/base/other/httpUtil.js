angular.module('app').factory('http', ['$http','$state', '$rootScope', function($http,$state,$rootScope) {
  	var cacheUtil = {
        put: function (key, value) {
            try {
                if (window.localStorage) {
                    window.localStorage.removeItem(key);
                    window.localStorage.setItem(key, value);
                }
            } catch (e) {
            	alert("请关闭设置中无痕模式之后重试")
                console.log(e);
            }
        },
        get: function (key) {
            return window.localStorage.getItem(key);
        },
        remove: function (key) {
            window.localStorage.removeItem(key);
        },        
        //存储对象，以JSON格式存储
        setObject:function(key,value){
          window.localStorage[key]=JSON.stringify(value);
        },        
        //读取对象
        getObject: function (key) {
          return JSON.parse(window.localStorage[key] || '{}');
        }
    };
  	
  	return {
  		signin: function(reqUrl, body, callback){
  				reqUrl =  globalConfig.web + reqUrl

	        	var headers = {'Content-Type': 'application/json'};
	        	$http({
	                method: 'POST',
	                data: body,
	                url: reqUrl,
	                timeout: 100000,
	                headers: headers
	            }).success(function(data, status){
	                callback(data, status);
	            }).error(function(data,status){
	                callback(data, status);
	            });
  		},
  		post: function(reqUrl, body, callback){
  			reqUrl = globalConfig.web + reqUrl;
	        	var headers = {'Content-Type': 'application/json'};
	        	headers.Authorization = "Bearer "+cacheUtil.get("Authorization");
	        	$http({
	                method: 'POST',
	                data: body,
	                url: reqUrl,
	                timeout: 100000,
	                headers: headers
	            }).success(function(data, status){
	            	if(data.status==200)
	                	callback(data, status);
	                else{
	                	alert(data.msg)
	                	callback(false);
	                }
	            }).error(function(data,status){
	            	if(status==401){
						cacheUtil.remove("Authorization")
						cacheUtil.remove("header")
						$rootScope.header = false;
					}
	                callback(false);
	            });
  		},
  		get: function(reqUrl, callback){
  			reqUrl = globalConfig.web + reqUrl;
	        	var headers = {'Content-Type': 'application/json'};
	        	headers.Authorization = "Bearer "+cacheUtil.get("Authorization");
	        	$http({
	                method: 'GET',
	                url: reqUrl,
	                timeout: 100000,
	                headers: headers
	            }).success(function(data, status){
	                if(data.status==200)
	                	callback(data, status);
	                else{
	                	alert(data.msg)
	                	callback(false);
	                }
	            }).error(function(data,status){
	            	if(status==401){
						cacheUtil.remove("Authorization")
						cacheUtil.remove("header")
						$rootScope.header = false;
					}
	                callback(false);
	            });
  		},
  		download: function(reqUrl, name){
  				var xhr = new XMLHttpRequest();
          		xhr.responseType = "blob";
          		xhr.open("GET", reqUrl, true);
          		xhr.setRequestHeader("Authorization", 'Bearer ' + cacheUtil.get("Authorization"));
  
		        xhr.onreadystatechange = function (e) {
		            if (this.readyState == 4) {
		                var response = this.response;
		                console.log(this)
		                var URL = window.URL || window.webkitURL || window;
		                var link = document.createElement('a');
		                link.href = URL.createObjectURL(response);
		                link.download = name;
		                var event = document.createEvent('MouseEvents');
		                event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		                link.dispatchEvent(event);
		            }
		        }
		        xhr.send(null);  
  		},
  		cacheUtil: cacheUtil 
  	}
}]);