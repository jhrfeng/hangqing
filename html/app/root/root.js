angular.module('app', ['ui.router','oc.lazyLoad']).config(['$stateProvider', function ($stateProvider) {
    $stateProvider
        .state('app', {
            url: "/app",
            templateUrl:"app/home/views/home.html",
            controller: 'rootCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    lazyloadlib.push('app/root/ctrl/rootCtrl.js');
                    return $ocLazyLoad.load(lazyloadlib);
                }]
            }
        });
}]);



