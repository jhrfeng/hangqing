globalConfig.app.config(function ($stateProvider) { 
    $stateProvider
        .state('home',{
            url:'/home',
            templateUrl: 'app/home/views/home.html',
            controller: 'homeCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/home/ctrl/homeCtrl.js',
                    ]);
                }]
            }
        })
        .state('plist',{
            url:'/plist',
            templateUrl: 'app/home/views/plist.html',
            controller: 'plistCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/home/ctrl/plistCtrl.js',
                    ]);
                }]
            }
        })
        .state('pdetail',{
            url:'/pdetail',
            templateUrl: 'app/home/views/pdetail.html',
            controller: 'pdetailCtrl',
            resolve: {
                load: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'app/home/ctrl/pdetailCtrl.js',
                    ]);
                }]
            }
        })
        
});
