angular.module('app').controller('homeCtrl', 
['$scope', '$rootScope', 'http', '$interval',
function($scope, $rootScope,http, $interval) 
{	
	$scope.news = {}

	list()

	var timer = $interval(
	  	function() {
	    	list()
	  	}, 5000);
	
		$scope.$on(
	  		"$destroy",
	  	function() {
	    	$interval.cancel( timer );
	  	}
	);

	function list(){
		http.get('/hangqing/hotlist', function(result, status){
			if(result)
				$scope.list = result.list;
		})

		http.get('/hangqing/wkb', function(result, status){
			console.log(result)
			if(result)
				$scope.wkb = result.data;
		})
	}
	

	$scope.getClass = function(index){
		if(index%2==0)
			return 'success1'
		if(index%2==1)
			return 'info1'
		// if(index%3==2)
		// 	return 'info1'
	}

	$scope.getPercent = function(per){
		if(Number(per) > 0){
			return 'p-1'
		}else{
			return 'p-3'
		}
	}

	$scope.addNew = function(){
		http.post('/hangqing/contact', $scope.news, function(result, status){
			if(result)
				alert('提交成功')
		})
	}

	http.get('/home/vist?source=0', function(result, status){})

	
}]);