angular.module('app').controller('plistCtrl', 
['$scope', '$rootScope', 'http', '$interval',
function($scope, $rootScope, http, $interval) 
{	
	$scope.query = {pages:0, 
					count:5,
					symbol:'',
					sorName:'',
					sorVal:''
					// price_cny:'', 
					// market_cap_cny:'', 
					// available_supply:'', 
					// available_supply_d:'',
					};

	list()

	var timer = $interval(
	  	function() {
	    	list()
	  	}, 50000);
	
		$scope.$on(
	  		"$destroy",
	  	function() {
	    	$interval.cancel( timer );
	  	}
	);

	function list(){
		http.post('/hangqing/blist', $scope.query, function(result, status){
			if(result){
				$scope.list = result.list;
				$scope.query.count = result.count;
			}
		})
	}

	$scope.sort = function(name, value){
		$scope.query.sorName = name
		$scope.query.sorVal  = value
		list()
	}

	$scope.default = function(){
		$scope.query = {pages:0, price_cny:'', market_cap_cny:'', available_supply:'', symbol:'', count:5};
		list()
	}

	$scope.search = function(){
		$scope.query.pages = 0;
		list()
	}
	
	$scope.page = function(index){
		if(index=='p'){
			if($scope.query.pages > 0){
				--$scope.query.pages
				list()
			}
		}else if(index=='n'){
			if($scope.query.pages < $scope.query.count){
				++$scope.query.pages
				list()
			}
		}else{
			$scope.query.pages = Number(index)
			list()
		}
		
	}

	$scope.getClass = function(index){
		if(index%2==0)
			return 'success1'
		if(index%2==1)
			return 'info1'
		// if(index%3==2)
		// 	return 'info1'
	}

	$scope.getPercent = function(per){
		if(Number(per) > 0){
			return 'p-1'
		}else{
			return 'p-3'
		}
	}
	
}]);